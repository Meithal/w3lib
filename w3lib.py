"""Library that helps to work with warcraft 3 maps"""
import re
import typing
import dataclasses
import struct
import hashlib
import abc
import datetime
import io

import mypyq
import dateutil.parser
import sylk_parser

import casc

_WAR3MAGIC = b"HM3W"


class CommonFile(typing.NamedTuple):
    filename: bytes
    used_for: str
    mandatory: bool

    def __bytes__(self):
        return self.filename


# war3mapImported\*.* war3campaignImported\*.* replaceableTextures\*.*
thumb_file = CommonFile(
    filename=b'war3mapMap.blp', used_for='Minimap preview', mandatory=False)
"war3mapMap.blp: Minimap preview"

extra_thumb_file = CommonFile(
    filename=b'war3mapPreview.tga', used_for='Custom map preview',
    mandatory=False)
"war3mapPreview.tga: Custom map preview"

alternate_thumb = CommonFile(
    filename=b'war3mapMap.b00', used_for='Alternate thumbnail', mandatory=False)
"war3mapMap.b00: Alternate thumbnail"

tga_thumb = CommonFile(
    filename=b'war3mapMap.tga', used_for='TGA thumbnail', mandatory=False)
"war3mapMap.tga: TGA thumbnail"

script_file = CommonFile(
    filename=b'war3map.j', used_for='Map script', mandatory=False)
"war3map.j: Map script"

alternate_script_file = CommonFile(
    filename=rb'scripts\war3map.j',
    used_for='Alternate map script',
    mandatory=False
)
"scripts\\war3map.j: Alternate map script"

lua_script_file = CommonFile(
    filename=b'war3map.lua', used_for='Lua map script', mandatory=False)
"war3map.lua: Lua map script"

alternate_lua_script_file = CommonFile(
    filename=rb'scripts\war3map.lua', used_for='Alternate lua script',
    mandatory=False)
"scripts\\war3map.lua: Alternate lua script"

map_strings_file = CommonFile(
    filename=b'war3map.wts', used_for='Map strings', mandatory=False)
"war3map.wts: strings"

map_info_file = CommonFile(
    filename=b'war3map.w3i', used_for='Map Info File', mandatory=True)
"war3map.w3i: Map Info File"

signature_file = CommonFile(
    filename=mypyq.signature_name, used_for='Archive integrity signature',
    mandatory=False)
"(signature): Archive integrity signature"

files_attributes_file = CommonFile(
    filename=mypyq.attributes_name,
    used_for='Archive files metadata',
    mandatory=False
)
"(attributes): Archive files metadata"

list_file = CommonFile(
    filename=mypyq.listfile_name, used_for='List of archive files',
    mandatory=False)
"(listfile): List of archive files"

tile_set = CommonFile(
    filename=b'war3map.w3e', used_for='Tile set', mandatory=True)
"war3map.w3e: Tile set"

triggers = CommonFile(
    filename=b'war3map.wtg', used_for='Map trigger names', mandatory=True)
"war3map.wtg: Map trigger names"

triggers_custom_text = CommonFile(
    filename=b'war3map.wct', used_for='Describes triggers', mandatory=True)
"war3map.wct: Describes triggers"

shadows = CommonFile(
    filename=b'war3map.shd', used_for='Map shadow bitmap', mandatory=True)
"war3map.shd: Map shadow bitmap"

minimap_extra = CommonFile(
    filename=b'war3map.mmp',
    used_for='Thumbnail extra, goldmines, start locations, neutrals...',
    mandatory=True
)
"war3map.mmp: Thumbnail extra, goldmines, start locations, neutrals..."

obsolete_paths = CommonFile(
    filename=b'war3mapPath.tga', used_for='Pre 1.21 paths', mandatory=False)
"war3mapPath.tga: Pre 1.21 paths"

paths = CommonFile(
    filename=b'war3map.wpm',
    used_for='Walking, flying, buildable,etc. paths',
    mandatory=True
)
"war3map.wpm: Walking, flying, buildable,etc. paths"

trees = CommonFile(
    filename=b'war3map.doo', used_for='Tree doodads of the map', mandatory=True)
"war3map.doo: Tree doodads of the map"

units = CommonFile(
    filename=b'war3mapUnits.doo',
    used_for='Units, items, placed at map start',
    mandatory=True
)
"war3mapUnits.doo: Units, items, placed at map start"

regions = CommonFile(
    filename=b'war3map.w3r', used_for='Regions', mandatory=True)
"war3map.w3r: Regions"

cameras = CommonFile(
    filename=b'war3map.w3c', used_for='Cameras', mandatory=True)
"war3map.w3c: Cameras"

sounds = CommonFile(
    filename=b'war3map.w3s', used_for='Sounds', mandatory=True)
"war3map.w3s: Sounds"

custom_units = CommonFile(
    filename=b'war3map.w3u', used_for='Custom units', mandatory=True)
"war3map.w3u: Custom units"

custom_items = CommonFile(
    filename=b'war3map.w3t', used_for='Custom items', mandatory=True)
"war3map.w3t: Custom items"

custom_abilities = CommonFile(
    filename=b'war3map.w3a', used_for='Custom abilities', mandatory=True)
"war3map.w3a: Custom abilities"

custom_destructables = CommonFile(
    filename=b'war3map.w3b', used_for='Custom destructables', mandatory=True)
"war3map.w3b: Custom destructables"

custom_doodads = CommonFile(
    filename=b'war3map.w3d', used_for='Custom doodads', mandatory=True)
"war3map.w3d: Custom doodads"

custom_buffs = CommonFile(
    filename=b'war3map.w3h', used_for='Custom buffs', mandatory=True)
"war3map.w3h: Custom buffs"

custom_upgrades = CommonFile(
    filename=b'war3map.w3q', used_for='Custom upgrades', mandatory=True)
"war3map.w3q: Custom upgrades"

archived_customs = CommonFile(
    filename=b'war3map.w3o',
    used_for='All custom data exported in a single file',
    mandatory=False
)
"war3map.w3o: All custom data exported in a single file"

gameplay_constants = CommonFile(
    filename=b'war3mapMisc.txt', used_for='Constants', mandatory=False)
"war3mapMisc.txt: Constants"

skin = CommonFile(
    filename=b'war3mapSkin.txt', used_for='Custom interface', mandatory=False)
"war3mapSkin.txt: Custom interface"

extra_info = CommonFile(
    filename=b'war3mapExtra.txt',
    used_for='Extra info, skybox, time of day, external resources',
    mandatory=False)
"war3mapExtra.txt: Extra info, skybox, time of day, external resources"

imported = CommonFile(
    filename=b'war3map.imp', used_for='Imported assets', mandatory=False)
"war3map.imp: Imported assets"

ai = CommonFile(
    filename=b'war3map.wai', used_for='Artificial intelligence',
    mandatory=False)
"war3map.wai: Artificial intelligence"

common_script_files = (
    script_file, alternate_script_file,
    lua_script_file, alternate_lua_script_file
)

common_strings_files = (
    map_strings_file,
)

common_info_files = (
    map_info_file,
)

common_files_list: typing.Tuple[CommonFile, ...] \
    = (
          signature_file,
          files_attributes_file,
          list_file,
          tile_set,
          triggers,
          triggers_custom_text,
          shadows,
          thumb_file,
          alternate_thumb,
          tga_thumb,
          extra_thumb_file,
          minimap_extra,
          obsolete_paths, paths,
          trees,
          units,
          regions,
          cameras,
          sounds,
          custom_units,
          custom_items,
          custom_abilities,
          custom_destructables,
          custom_doodads,
          custom_upgrades,
          custom_buffs,
          archived_customs,
          gameplay_constants,
          skin,
          extra_info,
          imported,
          ai,
      ) + common_script_files + common_strings_files + common_info_files

common_files: typing.Dict[bytes, CommonFile] = {
    obj.filename: obj
    for obj in common_files_list
}

order = [  # the order where conflicting files have been found in existing maps
    map_strings_file,
    paths,

    map_info_file,
    cameras,
    files_attributes_file,
]

common_campaign_list = [
    b'(listfile)',
    b'(signature)',
    b'(attributes)',
    b'war3campaign.w3u',
    b'war3campaign.w3t',
    b'war3campaign.w3a',
    b'war3campaign.w3b',
    b'war3campaign.w3d',
    b'war3campaign.w3q',
    b'war3campaign.w3f',
    b'war3campaign.imp',
    br'war3campaignImported\*.*',
]

model_formats = [b'mdl', b'mdx']
raster_formats = [b'bmp', b'tga', b'png', b'jpg', b'jpeg', b'pcx', b'blp',
                  b'dds']
# dds = blp for reforged
sound_formats = [b'wav', b'mp3', b'flac', b'ogg']  # flac = wav for reforged
font_formats = [b'ttf']
ai_formats = [b'wai']
misc_formats = [b'txt']
video_formats = [b'avi']  # unlikely to be found in a map,
# but observed in game files

supported_formats = \
    model_formats + \
    raster_formats + \
    sound_formats + \
    font_formats + \
    ai_formats + \
    misc_formats

strings_cache: typing.Dict[str, typing.Dict[str, 'StringVal']] = {}


def extract(mpq: mypyq.MPQArchive, stream: typing.BinaryIO,
            files: typing.Tuple[CommonFile, ...]):
    for file in files:
        ext, errors = mpq.read_file(stream, mypyq.FilePath(file.filename))
        if not errors:
            return ext
        else:
            return str(errors).encode('latin')


def _common_files(mpq: mypyq.MPQArchive, stream: typing.BinaryIO) -> \
        typing.Dict[bytes, list]:
    found = {}
    for file in common_files_list:
        _, errors = mpq.read_file(
            stream,
            mypyq.FilePath(file.filename),
            reason="w3lib common files list"
        )
        found[file.filename] = errors

    return found


def existing_files(mpq: mypyq.MPQArchive, stream: typing.BinaryIO) -> \
        typing.Iterator[bytes]:
    """Yields all the file paths we have found in a map archive,
    should be used with set() to remove possible duplicates."""
    for name, errors in _common_files(mpq, stream).items():
        if not errors:
            yield name

    list_file_, errors = mpq.read_file(
        stream, mypyq.FilePath(list_file.filename)
    )

    mpq_files = tuple()
    if not errors:
        mpq_files = tuple(
            bytearray(e)
            for e in list_file_.splitlines()
            if e and e.isascii()
        )

    for file_ in mpq_files:
        if b'\000' in file_:
            if file_.count(b'\000') > 1:
                file_.replace(b'\0', b'')
            else:
                file_[file_.index(0x00)] = ord(b'.')

        if mpq.test_filename(
                mypyq.FilePath(bytes(file_)),
                "found in list file"
        ):
            yield bytes(file_)

    script = extract(mpq, stream, common_script_files)
    if script:
        for imp in re.findall(
                rb"\"(war3mapImported.+?)\"",
                script,
                re.MULTILINE | re.IGNORECASE
        ):
            imp = imp.decode(
                'unicode_escape').encode()  # remove double backslashes
            if mpq.test_filename(
                    mypyq.FilePath(imp),
                    "found in script file with war3mapImported prefix."
            ):
                yield imp
        for imp in map(lambda x: x[0] + x[1], re.findall(
                rb"\"([^\"]+?\.)(" + b'|'.join(supported_formats) + b")\"",
                script,
                re.MULTILINE | re.IGNORECASE
        )):
            imp = imp.decode(
                'unicode_escape').encode()  # remove double backslashes
            if mpq.test_filename(
                    mypyq.FilePath(imp),
                    "Found in script file with common file extension."
            ):
                yield imp
    elements = get_custom_elements(mpq, stream)
    found = elements.units.imported_elements \
            | elements.items.imported_elements \
            | elements.destructables.imported_elements \
            | elements.doodads.imported_elements \
            | elements.upgrades.imported_elements \
            | elements.abilities.imported_elements \
            | elements.buffs.imported_elements

    for imp in found:
        if mpq.test_filename(
                mypyq.FilePath(imp),
                "found in the custom unit/item/etc. file"
        ):
            yield imp
        if imp.endswith(b'.mdl') \
                and mpq.test_filename(
            mypyq.FilePath(imp[:-3] + b'mdx'),
            "testing with mdx for a mdl"
        ):
            yield imp[:-3] + b'mdx'


def string_sizes(stream: bytes, start, count) -> typing.Tuple[int, ...]:
    sizes = []
    cursor = start
    for _ in range(count):
        start = cursor
        try:
            while stream[cursor] != 0:
                cursor += 1
        except IndexError:
            return tuple()
        sizes.append(1 + cursor - start)
        cursor += 1
    return tuple(sizes)


_T = typing.TypeVar('_T', bound=typing.Type[dataclasses.dataclass])


class _CleanString(abc.ABC):
    """Clean string fields from final NUL and non-printable bytes"""

    def __post_init__(self: _T, *args, **kwargs):
        for field in dataclasses.fields(self):
            if field.type is str:
                value = getattr(self, field.name)
                setattr(
                    self,
                    field.name,
                    ''.join(
                        filter(
                            lambda x: x.isprintable(), value.decode()
                        )
                    ).encode()
                )

    def replace_strings(self: _T, mpq, stream):
        strings_ = strings(mpq, stream)
        for field in dataclasses.fields(self):
            if field.type is str \
                    and getattr(self, field.name).startswith(b'TRIGSTR_'):
                val = getattr(self, field.name)[len(b'TRIGSTR_'):].decode()
                setattr(self, field.name, strings_[str(int(val))].value)


@dataclasses.dataclass
class MapInfo(_CleanString):
    file_format_version: int
    number_of_saves: int
    editor_version: int
    map_name: str
    map_author: str
    map_description: str
    recommended_players: str
    camera_bounds: list = b''
    camera_bounds_complement: list = b''
    playable_width: int = b''
    playable_height: int = b''
    flags: int = b''
    main_ground_type: str = b''
    loading_screen_number: int = b''
    custom_load_screen_path: str = b''
    load_screen_text: str = b''
    load_screen_title: str = b''
    load_screen_subtitle: str = b''
    game_data_set: int = b''
    prologue_screen_path: str = b''
    prologue_screen_text: str = b''
    prologue_screen_title: str = b''
    prologue_screen_subtitle: str = b''
    fog_id: int = b''
    fog_start_z: float = b''
    fog_end_z: float = b''
    fog_density: float = b''
    fog_red: int = b''
    fog_green: int = b''
    fog_blue: int = b''
    fog_alpha: int = b''
    weather_id: int = b''
    custom_sound_environment: str = b''
    tileset_id_of_custom_light_environment: str = b''
    custom_water_red: int = b''
    custom_water_green: int = b''
    custom_water_blue: int = b''
    custom_water_alpha: int = b''
    max_players: int = b''
    players: list = b''
    max_forces: int = b''
    forces: list = b''
    max_upgrades: int = b''
    upgrades: list = b''
    max_techs: int = b''
    techs: list = b''
    max_random_units: int = b''
    random_units: list = b''
    max_random_items: int = b''
    random_items: list = b''


def map_info(mpq: mypyq.MPQArchive, stream: typing.BinaryIO) -> MapInfo:
    info = extract(mpq, stream, common_info_files)
    if not info:
        return MapInfo(
            0, 0, 0, "Malformed", "Malformed", "Malformed", "Malformed"
        )
    sz = string_sizes(info, struct.calcsize("III"), 4)
    if len(sz) != 4:
        return MapInfo(
            0, 0, 0, "Malformed", "Malformed", "Malformed", "Malformed"
        )
    fmt = "<III%ds%ds%ds%ds" % sz
    info = MapInfo(*struct.unpack(fmt, info[:struct.calcsize(fmt)]))

    info.replace_strings(mpq, stream)

    return info


class StringVal(typing.NamedTuple):
    extra_info: str
    value: str


def strings(mpq: mypyq.MPQArchive, stream: typing.BinaryIO) -> typing.Dict[
    str, StringVal]:
    map_strings = extract(mpq, stream, common_strings_files)

    try:
        hash_ = hashlib.md5(map_strings).hexdigest()
    except Exception as e:
        print(e)
        hash_ = None
    if hash_ in strings_cache:
        return strings_cache[hash_]

    find = re.findall(
        r"STRING (\d+).*?^(//.+?)?\s*{\s*(.*?)\s*}",
        map_strings.decode(errors='replace'),
        re.MULTILINE | re.DOTALL)

    strings_cache[hash_] = {
        key: StringVal(extra, value)
        for key, extra, value in find
    }

    return strings_cache[hash_]


def map_creation_date_raw(mpq: mypyq.MPQArchive, stream: typing.BinaryIO) \
        -> typing.Optional[datetime.datetime]:
    script = extract(mpq, stream, common_script_files)
    if script:
        match = re.search(
            r"// {3}Date: (.+?)\r?\n",
            script[:500].decode('latin'), re.MULTILINE
        )
        if match:
            return dateutil.parser.parse(match.group(1))
    return None


def map_creation_date(mpq: mypyq.MPQArchive, stream: typing.BinaryIO,
                      format_: str) \
        -> typing.Union[datetime.datetime, str, None]:
    raw = map_creation_date_raw(mpq, stream)
    if raw:
        return datetime.datetime.strftime(raw, format_)
    return None


def var_string_unpack(string: bytes, content: bytes) \
        -> typing.Tuple[typing.Any, ...]:
    res = bytearray()
    for _, v in enumerate(string):
        if v == ord(b'$'):
            length = string_sizes(content, struct.calcsize(bytes(res)), 1)[0]
            res += b"%ds" % length
        else:
            res += bytes([v])

    res = bytes(res)
    return struct.unpack(res, content[:struct.calcsize(res)])


@dataclasses.dataclass
class W3MHeader:
    _magic: str
    _unknown: int
    _map_name: str
    _flags: int
    _max_players: int
    flags_values: typing.ClassVar = {
        0x0001: "if 1 = hide minimap in preview screens",
        0x0002: "if 1 = modify ally priorities",
        0x0004: "if 1 = melee map",
        0x0008: "if 1 = playable map size was large and has never been reduced to medium",
        0x0010: "if 1 = masked area are partially visible",
        0x0020: "if 1 = fixed player setting for custom forces",
        0x0040: "if 1 = use custom forces",
        0x0080: "if 1 = use custom tech tree",
        0x0100: "if 1 = use custom abilities",
        0x0200: "if 1 = use custom upgrades",
        0x0400: "if 1 = map properties menu opened at least once since map creation",
        0x0800: "if 1 = show water waves on cliff shores",
        0x1000: "if 1 = show water waves on rolling shores"
    }

    def insight(self):
        flags = []
        for mask, desc in self.flags_values.items():
            if mask & self._flags:
                flags.append(f"{desc} ({hex(mask)})")
        return {
            'raw': repr(self),
            'flags': bin(self._flags),
            'hex flags': hex(self._flags),
            'all flags': flags
        }

    @property
    def name(self):
        return self._map_name[:-1]  # remove terminal null byte


def w3mh_header(mpq: mypyq.MPQArchive) -> W3MHeader:
    """
    Loads the warcraft 3 header of a war3 map,
    not the mpq header that you get from mypyq.
    """

    return W3MHeader(*var_string_unpack(b"<4sI$II", mpq.raw_pre_archive))


@dataclasses.dataclass
class CustomData:
    extension: str
    name: str
    object_ids_file: str
    mod_ids_file: str
    has_extended: bool
    object_ids: dict = dataclasses.field(default_factory=dict)
    mod_ids: dict = dataclasses.field(default_factory=dict)
    object_ids_table: list = dataclasses.field(default_factory=list)
    mod_ids_table: list = dataclasses.field(default_factory=list)

    def __post_init__(self):
        print("Post init SLK population")
        with casc.Casc("D:\\Warcraft III:w3") as raw_obj:
            self._populate(raw_obj, 'object_ids')
            self._populate(raw_obj, 'mod_ids')
        print("all populated")

    def _populate(self, raw_obj: casc.Casc, field: str):
        foo = raw_obj.open_file(
            f'war3.w3mod:{getattr(self, f"{field}_file").lower()}')

        status, file_ = foo
        if status:
            file_, content, actual_read = raw_obj.read_file(file_)
            raw_obj.close_file(file_)
            sylk_ = sylk_parser.sylk_parser.SYLK().parse(
                io.StringIO(content.decode('ascii')))
            raw = iter(sylk_)
            legends = next(raw)
            setattr(self, field, {it[0]: dict(zip(legends, it)) for it in raw})
            setattr(self, f"{field}_table", list(el for el in iter(sylk_)))

        else:
            print("Something wrong with",
                  f'war3.w3mod:{getattr(self, f"{field}_file").lower()}')


upstream_data = {
    'w3u': CustomData(
        'w3u', 'Unit',
        'Units\\UnitData.slk',
        'Units\\UnitMetaData.slk', False),
    'w3t': CustomData(
        'w3t', 'Item',
        'Units\\ItemData.slk',
        'Units\\UnitMetaData.slk', False),
    'w3b': CustomData(
        'w3b', 'Destructable',
        'Units\\DestructableData.slk',
        'Units\\DestructableMetaData.slk', False),
    'w3d': CustomData(
        'w3d', 'Doodads',
        'Doodads\\Doodads.slk',
        'Doodads\\DoodadMetaData.slk', True),
    'w3a': CustomData(
        'w3a', 'Ability',
        'Units\\AbilityData.slk',
        'Units\\AbilityMetaData.slk', True),
    'w3h': CustomData(
        'w3h', 'Buffs',
        'Units\\AbilityBuffData.slk',
        'Units\\AbilityBuffMetaData.slk', False),
    'w3q': CustomData(
        'w3q', 'Upgrade',
        'Units\\UpgradeData.slk',
        'Units\\UpgradeMetaData.slk', True)
}


@dataclasses.dataclass
class _CustomElements:
    raw: dataclasses.InitVar[bytes]
    kind: dataclasses.InitVar[str]
    version: int = 0
    tables: dict = dataclasses.field(default_factory=dict)
    imported_elements: typing.Set[bytes] = dataclasses.field(
        default_factory=set)
    bugged = False
    mod_types: typing.ClassVar = {
        0: "<i4s",
        1: "<f4s",
        2: "<f4s",
        3: "0c"
    }

    def __post_init__(self, raw: bytes, kind: str):
        buffer = io.BytesIO(raw)
        self.tables = {
            'blizzard': {
                'amount': None,
                'bugged': None,
                'entries': []
            },
            'user_defined': {
                'amount': None,
                'bugged': None,
                'entries': []
            }
        }

        if raw:
            self.version, self.tables['blizzard']['amount'] = struct.unpack(
                "<II", buffer.read(struct.calcsize("<II")))
            self.fill_table('blizzard', buffer, raw, kind)
            self.tables['user_defined']['amount'] = struct.unpack(
                "<I", buffer.read(struct.calcsize("<I")))[0]
            self.fill_table('user_defined', buffer, raw, kind)

    def fill_table(self, table: str, buffer, raw, kind):
        try:
            for i in range(self.tables[table]['amount']):
                self.tables[table]['entries'].append(dict(
                    zip(
                        ('origID', 'newID', 'mod_count', 'mods'),
                        list(struct.unpack("<4s4sI", buffer.read(
                            struct.calcsize("<4s4sI")))) + [
                            []
                        ])
                ))
                number = self.tables[table]['entries'][i]['mod_count']
                for j in range(number):
                    self.tables[table]['entries'][i]['mods'].append(dict(
                        zip(
                            ('modIDcode', 'type', 'value', 'end'),
                            struct.unpack("<4sI", buffer.read(
                                struct.calcsize("<4sI"))) + (None, None))
                    ))
                    if upstream_data[kind].has_extended:
                        mod = self.tables[table]['entries'][i]['mods'][j]
                        mod['variant'], mod['column'] = struct.unpack("<II",
                                                                      buffer.read(
                                                                          struct.calcsize(
                                                                              "<II")))
                    type_ = self.tables[table]['entries'][i]['mods'][j]['type']
                    format_ = self.mod_types[type_]
                    if type_ == 3:
                        format_ = f"<{string_sizes(raw, buffer.tell(), 1)[0]}s4s"
                    self.tables[table]['entries'][i]['mods'][j]['value'], \
                    self.tables[table]['entries'][i]['mods'][j][
                        'end'] = struct.unpack(
                        format_, buffer.read(
                            struct.calcsize(format_)
                        )
                    )
                    # if type_ == 3 and self.tables[table]['entries'][i]['mods'][j]['value'].startswith(b'war3mapImported'):
                    if type_ == 3:
                        self.imported_elements.add(
                            self.tables[table]['entries'][i]['mods'][j][
                                'value'][:-1])

        except (KeyError, struct.error) as e:
            self.bugged = True
            self.tables[table]['bugged'] = {
                'message': str(e), 'raw': raw, 'position': buffer.tell()
            }


@dataclasses.dataclass
class _CustomCollection:
    units: _CustomElements
    items: _CustomElements
    destructables: _CustomElements
    doodads: _CustomElements
    abilities: _CustomElements
    buffs: _CustomElements
    upgrades: _CustomElements

    def is_bugged(self):
        return any((
            self.units.bugged,
            self.items.bugged,
            self.destructables.bugged,
            self.doodads.bugged,
            self.abilities.bugged,
            self.buffs.bugged,
            self.upgrades.bugged,
        ))


class _CustomElementsProxy:
    cache: typing.ClassVar = {}

    def __getitem__(self, req: typing.Tuple[mypyq.MPQArchive, typing.BinaryIO]):
        mpq, stream = req
        if hash(mpq) not in self.cache:
            self.cache[hash(mpq)] = _CustomCollection(
                units=_CustomElements(
                    mpq.read_file(stream,
                                  mypyq.FilePath(custom_units.filename))[0],
                    'w3u'),
                items=_CustomElements(
                    mpq.read_file(stream,
                                  mypyq.FilePath(custom_items.filename))[0],
                    'w3t'),
                destructables=_CustomElements(
                    mpq.read_file(
                        stream, mypyq.FilePath(custom_destructables.filename))[
                        0],
                    'w3b'),
                doodads=_CustomElements(
                    mpq.read_file(stream,
                                  mypyq.FilePath(custom_doodads.filename))[0],
                    'w3d'),
                abilities=_CustomElements(
                    mpq.read_file(stream,
                                  mypyq.FilePath(custom_abilities.filename))[0],
                    'w3a'),
                buffs=_CustomElements(
                    mpq.read_file(stream,
                                  mypyq.FilePath(custom_buffs.filename))[0],
                    'w3h'),
                upgrades=_CustomElements(
                    mpq.read_file(stream,
                                  mypyq.FilePath(custom_upgrades.filename))[0],
                    'w3q'),
            )

        return self.cache[hash(mpq)]


_map_custom_elements = _CustomElementsProxy()


def get_custom_elements(map_: mypyq.MPQArchive,
                        stream: typing.BinaryIO) -> _CustomCollection:
    return _map_custom_elements[(map_, stream)]
